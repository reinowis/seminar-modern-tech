﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Application.Interface;
using Domain.Interface.Services;
using Domain.Entities;
namespace Application
{
    public class AppServiceActivity : AppServiceBase<activity>, IAppServiceActivity
    {
        private readonly IServiceActivity _serviceActivity;

        public AppServiceActivity(IServiceActivity serviceActivity)
            : base(serviceActivity)
        {
            _serviceActivity = serviceActivity;
        }

        /**
        public IEnumerable<Activity> ObterActivitysEspeciais()
        {
            return _serviceActivity.ObterActivitysEspeciais(_serviceActivity.GetAll());
        }
        **/
    }
}
