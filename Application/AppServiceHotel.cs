﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Application.Interface;
using Domain.Interface.Services;
using Domain.Entities;
namespace Application
{
    public class AppServiceHotel : AppServiceBase<hotel>, IAppServiceHotel
    {
        private readonly IServiceHotel _serviceHotel;

        public AppServiceHotel(IServiceHotel serviceHotel)
            : base(serviceHotel)
        {
            _serviceHotel = serviceHotel;
        }

        /**
        public IEnumerable<Hotel> ObterHotelsEspeciais()
        {
            return _serviceHotel.ObterHotelsEspeciais(_serviceHotel.GetAll());
        }
        **/
    }
}
