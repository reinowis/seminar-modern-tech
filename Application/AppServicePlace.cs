﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Application.Interface;
using Domain.Interface.Services;
using Domain.Entities;
namespace Application
{
    public class AppServicePlace : AppServiceBase<place>, IAppServicePlace
    {
        private readonly IServicePlace _servicePlace;

        public AppServicePlace(IServicePlace servicePlace)
            : base(servicePlace)
        {
            _servicePlace = servicePlace;
        }

        public string ShowImage(int? id)
        {
            return _servicePlace.ShowImage(id);
        }
        /**
        public IEnumerable<Place> ObterPlacesEspeciais()
        {
            return _servicePlace.ObterPlacesEspeciais(_servicePlace.GetAll());
        }
        **/
    }
}
