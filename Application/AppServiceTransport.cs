﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Application.Interface;
using Domain.Interface.Services;
using Domain.Entities;
namespace Application
{
    public class AppServiceTransport : AppServiceBase<transport>, IAppServiceTransport
    {
        private readonly IServiceTransport _serviceTransport;

        public AppServiceTransport(IServiceTransport serviceTransport)
            : base(serviceTransport)
        {
            _serviceTransport = serviceTransport;
        }

        /**
        public IEnumerable<Transport> ObterTransportsEspeciais()
        {
            return _serviceTransport.ObterTransportsEspeciais(_serviceTransport.GetAll());
        }
        **/
    }
}
