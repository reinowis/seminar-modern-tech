﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Domain.Entities;
namespace Application.Interface
{
    public interface IAppServicePlace : IAppServiceBase<place>
    {
        string ShowImage(int? id);
    }
}
