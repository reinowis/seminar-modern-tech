﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
namespace Domain.Interface.Services
{
    public interface IServicePlace : IServiceBase<place>
    {
        string ShowImage(int? id);
        ///IEnumerable<place> ObterClientesEspeciais(IEnumerable<place> places);
    }
}
