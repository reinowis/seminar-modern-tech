﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interface.Services;
using Domain.Interface.Repositories;
using Domain.Entities;
namespace Domain.Services
{
   public class ServiceActivity : ServiceBase<activity>, IServiceActivity
    {
        private readonly IRepositoryActivity _repositoryActivity;

        public ServiceActivity(IRepositoryActivity repositoryActivity)
            : base(repositoryActivity)
        {
            _repositoryActivity = repositoryActivity;
        }
    }
}
