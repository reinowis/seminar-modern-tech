﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interface.Services;
using Domain.Interface.Repositories;
using Domain.Entities;
namespace Domain.Services
{
   public class ServiceHotel : ServiceBase<hotel>, IServiceHotel
    {
        private readonly IRepositoryHotel _repositoryHotel;

        public ServiceHotel(IRepositoryHotel repositoryHotel)
            : base(repositoryHotel)
        {
            _repositoryHotel = repositoryHotel;
        }
    }
}
