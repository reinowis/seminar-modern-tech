﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interface.Services;
using Domain.Interface.Repositories;
using Domain.Entities;
namespace Domain.Services
{
   public class ServicePlace : ServiceBase<place>, IServicePlace
    {
        private readonly IRepositoryPlace _repositoryPlace;

        public ServicePlace(IRepositoryPlace repositoryPlace)
            : base(repositoryPlace)
        {
            _repositoryPlace = repositoryPlace;
        }
        public string ShowImage(int? id)
        {
            return _repositoryPlace.ShowImage(id);
        }
    }
}
