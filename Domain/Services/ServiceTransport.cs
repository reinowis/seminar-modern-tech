﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Interface.Services;
using Domain.Interface.Repositories;
using Domain.Entities;
namespace Domain.Services
{
   public class ServiceTransport : ServiceBase<transport>, IServiceTransport
    {
        private readonly IRepositoryTransport _repositoryTransport;

        public ServiceTransport(IRepositoryTransport repositoryTransport)
            : base(repositoryTransport)
        {
            _repositoryTransport = repositoryTransport;
        }
    }
}
