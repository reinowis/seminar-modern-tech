﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity.ModelConfiguration;
namespace Infra.EntityConfig
{
    class ActivityConfiguration : EntityTypeConfiguration<activity>
    {
        public ActivityConfiguration()
        {
            HasKey(p => p.id);

            Property(p => p.name)
                .IsRequired()
                .HasMaxLength(128);
            HasRequired(p => p.Place)
                .WithMany(t => t.activities)
                .HasForeignKey(p => p.placeId);
        }
    }
}
