﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity.ModelConfiguration;
namespace Infra.EntityConfig
{
    class HotelConfiguration : EntityTypeConfiguration<hotel>
    {
        public HotelConfiguration()
        {
            HasKey(p => p.id);

            Property(p => p.name)
                .IsRequired()
                .HasMaxLength(128);
            HasRequired(p => p.Place)
                .WithMany(t => t.hotels)
                .HasForeignKey(p => p.placeId);
        }
    }
}
