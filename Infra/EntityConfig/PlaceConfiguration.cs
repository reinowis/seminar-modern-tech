﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity.ModelConfiguration;
namespace Infra.EntityConfig
{
    class PlaceConfiguration : EntityTypeConfiguration<place>
    {
        public PlaceConfiguration()
        {
            HasKey(p => p.id);

            Property(p => p.name)
                .IsRequired()
                .HasMaxLength(128);

            Property(p => p.googleId)
                .IsRequired();
        }
    }
}
