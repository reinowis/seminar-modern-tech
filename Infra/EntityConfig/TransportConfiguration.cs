﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity.ModelConfiguration;
namespace Infra.EntityConfig
{
    class TransportConfiguration : EntityTypeConfiguration<transport>
    {
        public TransportConfiguration()
        {
            HasKey(p => p.id);

            Property(p => p.name)
                .IsRequired()
                .HasMaxLength(128);
            HasRequired(p => p.Arrival)
                .WithMany(t => t.arrivals)
                .HasForeignKey(p => p.arrivalId);
            HasRequired(p => p.Departure)
                .WithMany(t => t.departures)
                .HasForeignKey(p => p.departureId);
        }
    }
}
