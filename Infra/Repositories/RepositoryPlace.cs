﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interface.Repositories;
namespace Infra.Repositories
{
    public class RepositoryPlace : RepositoryBase<place>, IRepositoryPlace
    {
        public String ShowImage(int? id)
        {
            var place = Db.places.Find(id);
            byte[] image = place.image;
            var img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(image));
            return img;
        }
        
    }
}
