﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Application;
using Application.Interface;
using Domain;
using Domain.Entities;
using Domain.Interface;
using Domain.Interface.Services;
using Domain.Interface.Repositories;
using Domain.Services;
using Infra.Repositories;
using Ninject;
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Presentation.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Presentation.App_Start.NinjectWebCommon), "Stop")]

namespace Presentation.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;
    using Ninject.Modules;
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind(typeof(IAppServiceBase<>)).To(typeof(AppServiceBase<>));
            kernel.Bind<IAppServicePlace>().To<AppServicePlace>();
            kernel.Bind<IAppServiceActivity>().To<AppServiceActivity>();
            kernel.Bind<IAppServiceHotel>().To<AppServiceHotel>();
            kernel.Bind<IAppServiceTransport>().To<AppServiceTransport>();

            kernel.Bind(typeof(IServiceBase<>)).To(typeof(ServiceBase<>));
            kernel.Bind<IServicePlace>().To<ServicePlace>();
            kernel.Bind<IServiceActivity>().To<ServiceActivity>();
            kernel.Bind<IServiceHotel>().To<ServiceHotel>();
            kernel.Bind<IServiceTransport>().To<ServiceTransport>();

            kernel.Bind(typeof(IRepositoryBase<>)).To(typeof(RepositoryBase<>));
            kernel.Bind<IRepositoryPlace>().To<RepositoryPlace>();
            kernel.Bind<IRepositoryActivity>().To<RepositoryActivity>();
            kernel.Bind<IRepositoryHotel>().To<RepositoryHotel>();
            kernel.Bind<IRepositoryTransport>().To<RepositoryTransport>();
        }
    }
}