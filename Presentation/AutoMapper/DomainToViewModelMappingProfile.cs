﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Domain.Entities;
using Presentation.ViewModels;
namespace Presentation.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModel"; }
        }

        public DomainToViewModelMappingProfile()
        {
            CreateMap<PlaceViewModel, place>();
            CreateMap<ActivityViewModel, activity>();
            CreateMap<TransportViewModel, transport>();
            CreateMap<HotelViewModel, hotel>();
        }
    }
}