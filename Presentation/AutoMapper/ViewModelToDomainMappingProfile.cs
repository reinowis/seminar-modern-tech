﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Domain.Entities;
using Presentation.ViewModels;
namespace Presentation.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<place, PlaceViewModel>();
            CreateMap<activity, ActivityViewModel>();
            CreateMap<transport, TransportViewModel>();
            CreateMap<hotel, HotelViewModel>();
        }
    }
}