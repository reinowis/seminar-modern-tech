﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Entities;
using Application.Interface;
using AutoMapper;
using Presentation.ViewModels;
using System.Net;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAppServicePlace _appPlace;

        public HomeController(IAppServicePlace appPlace)
        {
            _appPlace = appPlace;
        }

        public ActionResult Index()
        {
            var placeViewModel = Mapper.Map<IEnumerable<place>, IEnumerable<PlaceViewModel>>(_appPlace.GetAll());
           return View(placeViewModel);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var place = _appPlace.GetById(id);
            var placeViewModel = Mapper.Map<place, PlaceViewModel>(place);
            if (placeViewModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.imgSrc = _appPlace.ShowImage(id);
            ViewBag.mapQ = HttpUtility.UrlEncode(placeViewModel.name);
            return View(placeViewModel);
        }
    }
}