﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Entities;
using Application.Interface;
using AutoMapper;
using Presentation.ViewModels;
namespace Presentation.Controllers
{
    [Authorize]
    public class activitiesController : Controller
    {
        private readonly IAppServiceActivity _appActivity;
        private readonly IAppServicePlace _appPlace;

        public activitiesController(IAppServiceActivity appActivity, IAppServicePlace appPlace)
        {
            _appActivity = appActivity;
            _appPlace = appPlace;
        }

        // GET: activities
        public ActionResult Index()
        {
            var activityViewModel = Mapper.Map<IEnumerable<activity>, IEnumerable<ActivityViewModel>>(_appActivity.GetAll());
            return View(activityViewModel);
        }

        // GET: activities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var activity = _appActivity.GetById(id);
            var activityViewModel = Mapper.Map<activity, ActivityViewModel>(activity);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activityViewModel);
        }

        // GET: activities/Create
        public ActionResult Create()
        {
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name");
            return View();
        }

        // POST: activities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ActivityViewModel activity)
        {
            if (ModelState.IsValid)
            {
                var domainActivity = Mapper.Map<ActivityViewModel, activity>(activity);
                _appActivity.Add(domainActivity);
                return RedirectToAction("Index");
            }
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name", activity.placeId);
            return View(activity);
        }

        // GET: activities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var activity = _appActivity.GetById(id);
            var activityViewModel = Mapper.Map<activity, ActivityViewModel>(activity);
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name", activityViewModel.placeId);
            return View(activityViewModel);
        }

        // POST: activities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ActivityViewModel activity)
        {
            if (ModelState.IsValid)
            {
                var domainActivity = Mapper.Map<ActivityViewModel, activity>(activity);
                _appActivity.Update(domainActivity);
                return RedirectToAction("Index");
            }
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name", activity.placeId);
            return View(activity);
        }

        // GET: activities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var activity = _appActivity.GetById(id);
            var activityViewModel = Mapper.Map<activity, ActivityViewModel>(activity);

            return View(activityViewModel);
        }

        // POST: activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var activity = _appActivity.GetById(id);
            _appActivity.Remove(activity);
            return RedirectToAction("Index");
        }

    }
}
