﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Entities;
using Application.Interface;
using AutoMapper;
using Presentation.ViewModels;
namespace Presentation.Controllers
{
    [Authorize]
    public class hotelsController : Controller
    {
        private readonly IAppServiceHotel _appHotel;
        private readonly IAppServicePlace _appPlace;

        public hotelsController(IAppServiceHotel appHotel, IAppServicePlace appPlace)
        {
            _appHotel = appHotel;
            _appPlace = appPlace;
        }

        // GET: hotels
        public ActionResult Index()
        {
            var hotelViewModel = Mapper.Map<IEnumerable<hotel>, IEnumerable<HotelViewModel>>(_appHotel.GetAll());
            return View(hotelViewModel);
        }

        // GET: hotels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var hotel = _appHotel.GetById(id);
            var hotelViewModel = Mapper.Map<hotel, HotelViewModel>(hotel);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotelViewModel);
        }

        // GET: hotels/Create
        public ActionResult Create()
        {
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name");
            return View();
        }

        // POST: hotels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HotelViewModel hotel)
        {
            if (ModelState.IsValid)
            {
                var domainHotel = Mapper.Map<HotelViewModel, hotel>(hotel);
                _appHotel.Add(domainHotel);
                return RedirectToAction("Index");
            }
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name", hotel.placeId);
            return View(hotel);
        }

        // GET: hotels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var hotel = _appHotel.GetById(id);
            var hotelViewModel = Mapper.Map<hotel, HotelViewModel>(hotel);
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name", hotelViewModel.placeId);
            return View(hotelViewModel);
        }

        // POST: hotels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HotelViewModel hotel)
        {
            if (ModelState.IsValid)
            {
                var domainHotel = Mapper.Map<HotelViewModel, hotel>(hotel);
                _appHotel.Update(domainHotel);
                return RedirectToAction("Index");
            }
            ViewBag.placeId = new SelectList(_appPlace.GetAll(), "id", "name", hotel.placeId);
            return View(hotel);
        }

        // GET: hotels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var hotel = _appHotel.GetById(id);
            var hotelViewModel = Mapper.Map<hotel, HotelViewModel>(hotel);

            return View(hotelViewModel);
        }

        // POST: hotels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var hotel = _appHotel.GetById(id);
            _appHotel.Remove(hotel);
            return RedirectToAction("Index");
        }

    }
}
