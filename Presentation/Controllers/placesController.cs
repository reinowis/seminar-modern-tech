﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Entities;
using Application.Interface;
using AutoMapper;
using Presentation.ViewModels;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Authorize]
    public class placesController : Controller
    {
        private readonly IAppServicePlace _appPlace;

        public placesController(IAppServicePlace appPlace)
        {
            _appPlace = appPlace;
        }

        // GET: places
        public ActionResult Index()
        {
            var placeViewModel = Mapper.Map<IEnumerable<place>, IEnumerable<PlaceViewModel>>(_appPlace.GetAll());
            return View(placeViewModel);
        }
        
        // GET: places/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var place = _appPlace.GetById(id);
            var placeViewModel = Mapper.Map<place, PlaceViewModel>(place);
            if (placeViewModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.imgSrc = _appPlace.ShowImage(id);
            return View(placeViewModel);
        }

        // GET: places/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: places/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlaceViewModel place, HttpPostedFileBase ImageFile)
        {
            if (ModelState.IsValid)
            {
                if (ImageFile != null)
                {
                    byte[] buf = new byte[ImageFile.ContentLength];
                    ImageFile.InputStream.Read(buf, 0, buf.Length);
                    place.image = buf;
                }
                var domainPlace = Mapper.Map<PlaceViewModel, place>(place);
                _appPlace.Add(domainPlace);
                return RedirectToAction("Index");
            }

            return View(place);
        }

        // GET: places/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var place = _appPlace.GetById(id);
            var PlaceViewModel = Mapper.Map<place, PlaceViewModel>(place);
            ViewBag.imgSrc = _appPlace.ShowImage(id);
            return View(PlaceViewModel);
        }

        // POST: places/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlaceViewModel place, HttpPostedFileBase ImageFile)
        {
            if (ModelState.IsValid)
            {
                if (ImageFile != null)
                {
                    byte[] buf = new byte[ImageFile.ContentLength];
                    ImageFile.InputStream.Read(buf, 0, buf.Length);
                    place.image = buf;
                }
                var domainPlace = Mapper.Map<PlaceViewModel, place>(place);
                _appPlace.Update(domainPlace);
                return RedirectToAction("Index");
            }
            return View(place);
        }

        // GET: places/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var place = _appPlace.GetById(id);
            var PlaceViewModel = Mapper.Map<place, PlaceViewModel>(place);

            return View(PlaceViewModel);
        }

        // POST: places/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var place = _appPlace.GetById(id);
            _appPlace.Remove(place);
            return RedirectToAction("Index");
        }
        
    }
}
