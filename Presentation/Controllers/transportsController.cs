﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;
using Domain.Entities;
using Application.Interface;
using AutoMapper;
using Presentation.ViewModels;
namespace Presentation.Controllers
{
    [Authorize]
    public class transportsController : Controller
    {
        private readonly IAppServiceTransport _appTransport;
        private readonly IAppServicePlace _appPlace;

        public transportsController(IAppServiceTransport appTransport, IAppServicePlace appPlace)
        {
            _appTransport = appTransport;
            _appPlace = appPlace;
        }

        // GET: transports
        public ActionResult Index()
        {
            var transportViewModel = Mapper.Map<IEnumerable<transport>, IEnumerable<TransportViewModel>>(_appTransport.GetAll());
            return View(transportViewModel);
        }

        // GET: transports/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var transport = _appTransport.GetById(id);
            var transportViewModel = Mapper.Map<transport, TransportViewModel>(transport);
            if (transport == null)
            {
                return HttpNotFound();
            }
            return View(transportViewModel);
        }

        // GET: transports/Create
        public ActionResult Create()
        {
            ViewBag.departureId = new SelectList(_appPlace.GetAll(), "id", "name");
            ViewBag.arrivalId = new SelectList(_appPlace.GetAll(), "id", "name");
            return View();
        }

        // POST: transports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TransportViewModel transport)
        {
            if (ModelState.IsValid)
            {
                var domainTransport = Mapper.Map<TransportViewModel, transport>(transport);
                _appTransport.Add(domainTransport);
                return RedirectToAction("Index");
            }
            ViewBag.departureId = new SelectList(_appPlace.GetAll(), "id", "name", transport.departureId);
            ViewBag.arrivalId = new SelectList(_appPlace.GetAll(), "id", "name", transport.arrivalId);
            return View(transport);
        }

        // GET: transports/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var transport = _appTransport.GetById(id);
            var transportViewModel = Mapper.Map<transport, TransportViewModel>(transport);
            ViewBag.departureId = new SelectList(_appPlace.GetAll(), "id", "name", transportViewModel.departureId);
            ViewBag.arrivalId = new SelectList(_appPlace.GetAll(), "id", "name", transportViewModel.arrivalId);
            return View(transportViewModel);
        }

        // POST: transports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TransportViewModel transport)
        {
            if (ModelState.IsValid)
            {
                var domainTransport = Mapper.Map<TransportViewModel, transport>(transport);
                _appTransport.Update(domainTransport);
                return RedirectToAction("Index");
            }
            ViewBag.departureId = new SelectList(_appPlace.GetAll(), "id", "name", transport.departureId);
            ViewBag.arrivalId = new SelectList(_appPlace.GetAll(), "id", "name", transport.arrivalId);
            return View(transport);
        }

        // GET: transports/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var transport = _appTransport.GetById(id);
            var transportViewModel = Mapper.Map<transport, TransportViewModel>(transport);

            return View(transportViewModel);
        }

        // POST: transports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var transport = _appTransport.GetById(id);
            _appTransport.Remove(transport);
            return RedirectToAction("Index");
        }

    }
}
