﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Presentation.ViewModels
{
    public class HotelViewModel
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required(ErrorMessage = "Please supply the name")]
        [MaxLength(128, ErrorMessage = "Maximum {0} characters")]
        [MinLength(2, ErrorMessage = "Minimum {0} characters")]
        public string name { get; set; }

        [DisplayName("Place")]
        public int placeId { get; set; }

        
        public Nullable<int> price { get; set; }
        
        [MaxLength(256, ErrorMessage = "Maximum {0} characters")]
        [MinLength(2, ErrorMessage = "Minimum {0} characters")]
        public string contact { get; set; }
        
        [Required(ErrorMessage = "Please supply the address")]
        [MaxLength(128, ErrorMessage = "Maximum {0} characters")]
        [MinLength(2, ErrorMessage = "Minimum {0} characters")]
        [DisplayName("Address")]
        public string link { get; set; }

        public virtual PlaceViewModel Place { get; set; }
    }
}