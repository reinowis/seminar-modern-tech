﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Presentation.ViewModels
{
    public class PlaceViewModel
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required(ErrorMessage = "Please supply the name")]
        [MaxLength(128, ErrorMessage = "Maximum {0} characters")]
        [MinLength(2, ErrorMessage = "Minimum {0} characters")]
        public string name { get; set; }
        public Nullable<double> location_x { get; set; }
        public Nullable<double> location_y { get; set; }


        [Required(ErrorMessage = "Please supply the google ID")]
        [DisplayName("Google ID location:")]
        public string googleId { get; set; }

        [Required(ErrorMessage = "Please supply the description")]
        [MinLength(15, ErrorMessage = "Minimum {0} characters")]
        public string description { get; set; }
        public byte[] image { get; set; }
        public virtual ICollection<ActivityViewModel> activities { get; set; }
        public virtual ICollection<HotelViewModel> hotels { get; set; }
        public virtual ICollection<TransportViewModel> arrivals { get; set; }
        public virtual ICollection<TransportViewModel> departures { get; set; }
    }
}