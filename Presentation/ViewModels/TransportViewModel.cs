﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Presentation.ViewModels
{
    public class TransportViewModel
    {
        [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Required(ErrorMessage = "Please supply the departure")]
        [DisplayName("Departure")]
        public int departureId { get; set; }

        [Required(ErrorMessage = "Please supply the arrival")]
        [DisplayName("Arrival")]
        public int arrivalId { get; set; }

        [Required(ErrorMessage = "Please supply the name")]
        [MaxLength(128, ErrorMessage = "Maximum {0} characters")]
        [MinLength(2, ErrorMessage = "Minimum {0} characters")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please supply the timetable")]
        public string timetable { get; set; }
        public Nullable<int> price { get; set; }

        [ForeignKey("departureId")]
        public virtual PlaceViewModel Departure { get; set; }

        [ForeignKey("arrivalId")]
        public virtual PlaceViewModel Arrival { get; set; }
    }
}