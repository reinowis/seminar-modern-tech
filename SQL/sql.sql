USE [master]
GO
/****** Object:  Database [seminar]    Script Date: 12/21/2017 10:58:45 AM ******/
CREATE DATABASE [seminar]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'seminar', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\seminar.mdf' , SIZE = 6144KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'seminar_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\seminar_log.ldf' , SIZE = 4672KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [seminar] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [seminar].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [seminar] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [seminar] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [seminar] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [seminar] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [seminar] SET ARITHABORT OFF 
GO
ALTER DATABASE [seminar] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [seminar] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [seminar] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [seminar] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [seminar] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [seminar] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [seminar] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [seminar] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [seminar] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [seminar] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [seminar] SET  DISABLE_BROKER 
GO
ALTER DATABASE [seminar] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [seminar] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [seminar] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [seminar] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [seminar] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [seminar] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [seminar] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [seminar] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [seminar] SET  MULTI_USER 
GO
ALTER DATABASE [seminar] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [seminar] SET DB_CHAINING OFF 
GO
ALTER DATABASE [seminar] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [seminar] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [seminar]
GO
/****** Object:  Table [dbo].[activities]    Script Date: 12/21/2017 10:58:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[activities](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](128) NOT NULL,
	[placeId] [int] NOT NULL,
	[time_start] [time](7) NOT NULL,
	[time_end] [time](7) NOT NULL,
 CONSTRAINT [PK_activity_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hotels]    Script Date: 12/21/2017 10:58:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hotels](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](128) NOT NULL,
	[placeId] [int] NOT NULL,
	[price] [int] NULL,
	[contact] [nvarchar](256) NULL,
	[link] [nvarchar](256) NULL,
 CONSTRAINT [PK_hotels] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[places]    Script Date: 12/21/2017 10:58:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[places](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](128) NOT NULL,
	[location_x] [float] NULL,
	[location_y] [float] NULL,
	[googleId] [nchar](128) NULL,
	[description] [text] NULL,
	[image] [image] NULL,
 CONSTRAINT [PK_places] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[transports]    Script Date: 12/21/2017 10:58:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transports](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[departureId] [int] NOT NULL,
	[arrivalId] [int] NOT NULL,
	[name] [nvarchar](128) NOT NULL,
	[timetable] [nvarchar](256) NULL,
	[price] [int] NULL,
 CONSTRAINT [PK_transport] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user]    Script Date: 12/21/2017 10:58:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[id] [nchar](128) NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[password] [nchar](128) NOT NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[activities]  WITH CHECK ADD  CONSTRAINT [FK_activities_places] FOREIGN KEY([placeId])
REFERENCES [dbo].[places] ([id])
GO
ALTER TABLE [dbo].[activities] CHECK CONSTRAINT [FK_activities_places]
GO
ALTER TABLE [dbo].[hotels]  WITH CHECK ADD  CONSTRAINT [FK_hotels_places] FOREIGN KEY([placeId])
REFERENCES [dbo].[places] ([id])
GO
ALTER TABLE [dbo].[hotels] CHECK CONSTRAINT [FK_hotels_places]
GO
ALTER TABLE [dbo].[transports]  WITH CHECK ADD  CONSTRAINT [FK_transport_arrival] FOREIGN KEY([arrivalId])
REFERENCES [dbo].[places] ([id])
GO
ALTER TABLE [dbo].[transports] CHECK CONSTRAINT [FK_transport_arrival]
GO
ALTER TABLE [dbo].[transports]  WITH CHECK ADD  CONSTRAINT [FK_transport_depart] FOREIGN KEY([departureId])
REFERENCES [dbo].[places] ([id])
GO
ALTER TABLE [dbo].[transports] CHECK CONSTRAINT [FK_transport_depart]
GO
USE [master]
GO
ALTER DATABASE [seminar] SET  READ_WRITE 
GO
